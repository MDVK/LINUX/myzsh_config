# LIBS
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh
export LC_CTYPE="en_US.UTF-8"


#ZSH_THEME="robbyrussell"
ZSH_THEME="amuse"
#ZSH_THEME="bullet-train"

# PLUGIN
plugins=(git alias-tips zsh-completions almostontop zsh-autosuggestions pip )
plugins+=(history history-substring-search httpie sudo vagrant postgres tig)
plugins+=(osx lein node npm z mosh k zsh-syntax-highlighting)
# SC
source $ZSH/oh-my-zsh.sh

# Coloring
blue='\e[1;34m'
normal='\e[0;23m'
purple='\e[1;35m'
cyan='\e[1;36m'
red='\e[1;31m'
coklat='\e[0;33m'
ijo='\e[0;32m'
putih='\e[97m'
itam='\e[0;30m'
kuning='\e[1;33m'

# CONSTANT
LINE="────────────────────────────────────────────────"
SSID=`iwgetid -r`
WHOAMI='whoami'
APP="Պḓṽк💎"
#CPUSensor=`inxi -s | grep Temperatures | awk '{print $5,$6,$9,$10}' | cut -f1 -d,`
MYIP1=`nmcli | grep servers | awk '{print $2}'`
MYIP2=`nmcli | grep servers | awk '{print $3}'`
INTERFACE=`ifconfig | grep wl | awk '{print $1}' | cut -f1 -d:`
####
MYMAC=`cat /sys/class/net/$INTERFACE/address`
MYFREQ=`iwgetid -f  | awk '{print $2}' | cut -f2 -d:`
MYCH=`iwgetid -c | awk '{print $2}' | cut -f2 -d:`
####
FINSTALL=`ls -lact --full-time /etc |tail | grep services | awk '{print $6 }'`
KERVER=`uname -r`
UPTIME1=$(uptime | awk '{print $1}' | cut -f1 -d,)
UPTIME2=$(uptime | awk '{print $3}' | cut -f1 -d,)


## ECHO FIRST LOAD 
echo "Hi `$WHOAMI` | everything look good ?" #| lolcat -F 0.8

## MDVK ALIAS
alias z="inxi -s"
alias x="exit"
alias c="clear"
alias e="subl3"
alias ez="subl3 $HOME/.zshrc"
alias server="python -m SimpleHTTPServer"
alias socks="ssh -vND 8888 kim"
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias rankfast="rankmirrors -n 5 /etc/pacman.d/mirrorlist > mirrorlistbest"
alias sap="sudo create_ap wlp3s0 enp2s0f1 💎free@wifi" #Start Access Point
alias qwe="sudo airgeddon"
alias su='sudo su'
alias talias='sudo nano /home/croot/.bash_aliases'
alias zzz='ping 8.8.8.8'
alias kudet='sudo yaourt -Syua'
alias pasang='sudo pacman -S'
#alias hapus='sudo pacman -Rsc'
alias cari='yaourt --noconfirm '
alias asd='chromium 127.0.0.1/phpmyadmin'
alias reboot='sudo reboot'
alias ggg='/opt/genymobile/genymotion/genymotion'
alias gtop='gnome-system-monitor'
alias xampp='sudo /opt/lampp/lampp '
alias studio='/opt/android-studio/bin/studio.sh'
alias subl3='LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 subl3'
alias subl='LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 subl3'
alias sl='LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 subl3'

alias @='cat'

## CUSTOM FUNCTIONS
# Create a new directory and enter it
function mkd() {
        mkdir -p "$@" && cd "$_";
}
xs(){
	xcowsay "${1}"
}

# cat + Grab
f(){
	cat "${1}" | grep "${2}" 
}

npmi(){
	sudo npm install "${1}" --unsafe-perm=true --allow-root
}

mdvk() {
     curl "http://apaini-as.cloud.revoluz.io/checker.php?target=${1}" | python -m json.tool |grep -E '"username"|"password"|"mac"' 
}

qrcode(){
	printf "${1}" \ | curl -F-=\<- qrenco.de
}


# Git clone + npm install
function gcn {
    url=$1;
    if [ -n "${1}" ]; then
        echo 'OK'
    else
        echo 'Nooooooooo '
    fi
    cd ~/code;
    reponame=$(echo $url | awk -F/ '{print $NF}' | sed -e 's/.git$//');
    git clone $url $reponame;
    cd $reponame;
    npm install;    
}

 ## ============================ MODE ============================


function watchmon(){
	watch -n 0 cat "/proc/`pidof "${1}"`/status | grep Anon"
}

# DEV MODE | CODING TIME 
function dm() {		
	# TLP PERFORM
	sudo tlp start

	# NETBEANS
	sudo archlinux-java set java-8-openjdk
	# JAVA ANTI ALIASSING
	export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'
	# JAVA
	#export JAVA_HOME=$(/usr/libexec/java_home)
	export M2=$HOME/.m2
	export PATH=$PATH:$M2

	# Inotify Watches Limit
	sudo echo "fs.inotify.max_user_watches = 524288" >  /etc/sysctl.d/sysctl.conf
	sudo sysctl -p --system

	## GO
	#export GOPATH=$HOME/code/golang
	#export PATH=$GOPATH/bin:$PATH

	## PURE
	#autoload -U promptinit; promptinit
	#prompt pure
	
	# Make zsh know about hosts already accessed by SSH
	#zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'
	
	## GIT
	GIT_AUTHOR_NAME="Aditya Pratama"
	GIT_AUTHOR_EMAIL="adit@calonsultan.com"
	git config --global user.name "$GIT_AUTHOR_NAME"
	GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
	GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
	git config --global user.email "$GIT_AUTHOR_EMAIL"
	## NPM
	npm set init.author.email "Aditya Pratama"
	npm set init.author.name "mdvk"
	npm set init.license "MIT"
	## LINUX | APACHE | MYSQL | PHPMYADMIN |
	echo $LINE
	sudo /opt/lampp/lampp start
	echo $LINE
	echo $normal "JAVA ➡$ijo archlinux-java set java-8-openjdk" 
	echo $normal "GIT  ➡" $ijo$GIT_AUTHOR_NAME "➡" $GIT_AUTHOR_EMAIL
	echo $normal "NPM  ➡" $ijo$GIT_AUTHOR_NAME "➡ `$WHOAMI`" 
	echo $normal "EXPORT MODULE ➡$ijo JAVA / CLOJURE"
	echo $normal "EXPORT MODULE ➡$ijo GO"
	echo $normal "EXPORT MODULE ➡$ijo PURE"
	echo $normal "EXPORT MODULE ➡$ijo ZSHSTYLE"
	echo $normal$LINE

}

## ANIMATE : sleep 0.1 &&   | lolcat -F 0.7

echo  $normal$LINE
#echo  $normal		"Networking		➡"  $kuning$MYIP1 "|" $ijo$MYIP2 "|" $purple$MYMAC
echo  $putih		"Networking		➡" $red$INTERFACE "➡" $blue$SSID "➡" $kuning$MYMAC
echo  $putih		"System Uptime 		➡" $red$UPTIME1 "➡" $kuning$UPTIME2 Menit "➡$blue"  `$WHOAMI` #❤ "$APP" 
echo  $putih		"Kerner Version		➡" $KERVER "$purple (◣_◢)" 
# echo  $putih		"Temperatures		➡"$putih"$cyan "$red$CPUSensor 
echo  $putih		"First Install		➡"$kuning $FINSTALL "$purple (💎)"
echo  $putih		"Last Login		➡" $(last login | awk '{print $3,$4,$5,$6}')

echo  $normal$LINE
echo  " Type $blue(alias)$normal to see all shortcut"
echo  " Type $kuning(dm)$normal to run deploy mode"
echo  " Type $coklat(anon)$normal to run like anonymous"
echo  $normal$LINE
 
